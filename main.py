import mlflow
from sklearn.datasets import load_digits


def main():
    model = mlflow.sklearn.load_model("LGBMRegressorModel")
    X, y = load_digits(return_X_y=True)
    pred = model.predict(X)
    print(pred)


if __name__ == '__main__':
    main()
